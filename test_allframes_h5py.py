""" Para transformar desde h5 a un npy o para hacerlo con
la cantidad justas de filas (imagenes)
"""

import h5py
import numpy as np
import imageio
from PIL import Image



def color_grid_vis(X, nh, nw, save_path=None):
    h, w = X[0].shape[:2]
    img = np.zeros((h*nh, w*nw, 3))
    for n, x in enumerate(X):
        j = n/nw
        i = n%nw
        img[j*h:j*h+h, i*w:i*w+w, :] = x
    if save_path is not None:
        imageio.imwrite(save_path, img)
    return img


def make_generator(path, n_files, batch_size):
    epoch_count = [1]
    fp = h5py.File(path, 'r')
    all_unlab_data_h5 = fp["X"]
    def get_epoch():
        images = np.zeros((batch_size, 3, 64, 64), dtype='int32')
        files = range(n_files)
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1
        for n,idx in enumerate(files):
            image = all_unlab_data_h5[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                yield (images,)
    return get_epoch


file_path = '/home/jmesuro/videos_soja/robot_videos_MOV/test3/parches_ROIT2frames_robot_u_m.h5'
fp = h5py.File(file_path)
n_images = 26

l = []
i = 0
for p in fp['X']:
    print('image p numero ' + str(i))
    i = i+1
    l.append(p)

l.reverse()
jp = 0
i = 0
j = 0

imgs = []
for k in range(n_images):
    print('image numero ' + str(k))
    b = np.zeros(shape=(576, 1472, 3), dtype=np.uint8)
    for i in range(0, 9):
        for j in range(0, 23):
            im = l.pop()
            im = im.transpose(1, 2, 0)  # channel last
            b[64*i:64*(i+1), 64*j:64*(j+1), :] = im

    im = Image.fromarray(b)
    imgs.append(im)

for i in range(len(imgs)):
    imgs[i].save('/home/jmesuro/videos_soja/robot_videos_MOV/test3/image_' + str(i) + '.png')


fp.close()
