"""
Permite crear el dataset no etiquetado sin filtro, para el segundo etiquetado.
"""
import numpy as np
import os
import h5py

from utils_unlabeled import  list_img_files, load_img


VIDEOS_ROBOT_PATH = '/home/jmesuro/videos_soja/robot_videos_MOV/test4/'
OUT_PATH = '/home/jmesuro/videos_soja/robot_videos_MOV/test4/'
n_images = 4491
# Se crea la carpeta output si no existe
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)


def get_image_como_parches(ima):

    # ROI (0,0) --> (504,224)

    l = []
    for i in range(0, 9):
        for j in range(0, 23):
            l.append(ima[:, 504+i*64:504+(i+1)*64, 224+j*64: 224+(j+1)*64])
    return l


def mk_parches_h5(videos_robot_path=VIDEOS_ROBOT_PATH):

    fp = h5py.File(OUT_PATH + 'parches_ROIT2frames_robot_u.h5', 'w')
    fp.create_dataset('X', (207*n_images, 3, 64, 64), dtype=np.int8)

    jp = 0

    imgs, _ = list_img_files(videos_robot_path)
    for filename in imgs:
        print('Obteniendo parches de ' + filename)
        im = load_img(filename)
        ima = np.asarray(im).transpose((2, 0, 1))  # channel first
        parches = get_image_como_parches(ima)
        for imi in parches:
            fp['X'][jp] = imi
            jp = jp + 1

    fp.close()
    return

mk_parches_h5()
