"""Toma un dataset de imagenes con sus indices y etiquetas
y lo balancea. Balancea por la clase de menor cantidad.
Elige imagenes de las otras clases de manera aleatoria.

Returns:
    parches, labels, idx balanceado
"""

import numpy as np
import random

parches = np.load('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ROB_96_BALANCEADO_IDX/parches_labeled_(2058, 96, 96, 3)_uint8.npy')
labels = np.load('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ROB_96_BALANCEADO_IDX/labels_(2058, 3)_3_clases_float64_maleza.npy')
idx = (open('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ROB_96_BALANCEADO_IDX/idx_parches_labeled_(2058, 96, 96, 3)_uint8.npy')).readlines()


bolsa = range(0,parches.shape[0])
malezas = 0
suelos = 0
sojas = 0
balance = int(min(sum(labels)))

parches_balanceado = np.zeros(shape=(balance*3,96,96,3), dtype=np.uint8)
labels_balanceado =  np.zeros(shape=(balance*3,3), dtype=np.float64 )
idx_balanceado = [None]*balance*3

# [maleza, soja, suelo]


def balanceado():
    return malezas == balance and suelos == balance and sojas == balance

i_balanceado = 0
while not balanceado():
    ir = random.choice(bolsa)

    if labels[ir][0] == 1:  # es maleza
        if malezas == balance:
            continue
        malezas += 1
    if labels[ir][1] == 1:  # es suelo
        if sojas == balance:
            continue
        sojas += 1
    if labels[ir][2] == 1:  # es soja
        if suelos == balance:
            continue
        suelos += 1

    parches_balanceado[i_balanceado] = parches[ir]
    labels_balanceado[i_balanceado] = labels[ir]
    idx_balanceado[i_balanceado] = idx[ir]

    bolsa.remove(ir)
    i_balanceado += 1


np.save('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ROB_96_BALANCEADO_IDX/parches_labeled_(' + str(balance*3) +
        ',96,96,3)_uint8_BALANCEADO_TEST.npy', parches_balanceado)

np.save('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ROB_96_BALANCEADO_IDX/labels_(' + str(balance*3)
        + ',3)_3_clases_float64_BALANCEADO_TEST.npy', labels_balanceado)


np.savetxt('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ROB_96_BALANCEADO_IDX/idx.npy', idx_balanceado, fmt='%s',newline='')  # indice

