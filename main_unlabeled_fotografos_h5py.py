""" Permite armar los dataset de fotografos no etiquetados,
de parches y entornos.

A diferencia de los etiquetados que usan matrices numpy, acá se usa
h5py. Esto es por lo tamaños de los dataset no etiquetados que son
muy superiores, y se cargarn en memoria por batches.

Obtiene los frames de los videos y genera los datasets.
Hay dos opciones, guardando temporalmente los frames en disco
o permanetemente.


Tiene 5 funciones:
        mk_parches_from_video_h5    arma los dataset desde una capeta de video, no guarda los frames
        mk_entornos_from_video_h5   arma los dataset entornos una capeta de video, no guarda los frames
        mk_all_frames_fot           genera todos los frames desde los videos
        mk_parches_from_frames_h5   arma los dataset desde una capeta de frames

inputs
    VIDEOS_FOTOGRAFOS_PATH:
        Carpeta con los videos de fotógrafos
    FILTRO_FOTOGRAFOS_PATH:
        Indica los frames de que videos de fotógrafos deben usarse

outputs
    IXP_FILE_NAME:
        índice de los videos no etiquetados
    OUT_PATH:
        nombre del archivo h5py
    FRAME_PATH:
        carpeta con todos los frames de los VIDEOS_FOTOGRAFOS_PATH
"""

import numpy as np
import subprocess
import h5py

from utils_unlabeled import list_img_files, get_video_y_frame_fotografo, \
    load_img, get_parches, get_entornos, list_video_fotografos_files_filtrado

# INPUT
VIDEOS_FOTOGRAFOS_PATH = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/DJI_videos/'
FILTRO_FOTOGRAFOS_PATH = '/home/jmesuro/image-dataset-maker/input_unlabeled/filtro_videos_fotografos.txt'
# OUTPUT
IXP_FILE_NAME = '/home/jmesuro/image-dataset-maker/output_unlabeled/index_entornos_fotografos_u.npz'
OUT_PATH = '/home/jmesuro/image-dataset-maker/output_unlabeled/parches_fotografos_u2.h5'
FRAME_PATH = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/frames_fotografos_unlabeled'




def mk_parches_from_video_h5(size_parche, n_parches):
    """ Genera en disco el dataset para parches h5py
    temporalmente genera los frames en la misma carpeta
    donde están los videos.
    Args:
      n_parches:
        numero de parches por frame
    """
    fp = h5py.File(OUT_PATH, 'w')
    fp.create_dataset('X', (1500000, 3, size_parche, size_parche), dtype=np.uint8)

    ix_p = []
    jp = 0

    # Se filtran los videos
    videos_f, _ = list_video_fotografos_files_filtrado(FILTRO_FOTOGRAFOS_PATH, VIDEOS_FOTOGRAFOS_PATH)

    for videoname in videos_f:
        video_number = videoname[-8:-4]

        # Se obtienen los frames del video
        print('Obteniendo frames de ' + videoname)
        subprocess.getoutput('mkdir ' + VIDEOS_FOTOGRAFOS_PATH + '/' + video_number)
        s = 'ffmpeg -i ' + videoname + ' ' + '-s 1920x1080 ' + VIDEOS_FOTOGRAFOS_PATH + video_number + \
            '/thumb%04d.png -hide_banner'
        subprocess.getoutput(s)

        imgs, _ = list_img_files(VIDEOS_FOTOGRAFOS_PATH + video_number)
        for filename in imgs:
            video, frame = get_video_y_frame_fotografo(filename)
            print('Obteniendo parches de ' + filename)
            im = load_img(filename)
            ima = np.asarray(im).transpose((2, 0, 1))  # channel first
            parches, ix_p_aux = get_parches(ima, n_parches, size_parche, video, frame)
            for imi in parches:
                fp['X'][jp] = imi
                jp = jp + 1
            ix_p.extend(ix_p_aux)

        # Se borran los frames del disco
        subprocess.getoutput('rm -rf ' + VIDEOS_FOTOGRAFOS_PATH + video_number)

    np.savetxt(IXP_FILE_NAME, ix_p, fmt='%s')
    fp.close()
    return


def mk_entornos_from_video_h5(size_entorno, n_entornos):
    """ Genera en disco el dataset para entornos h5py
    temporalmente genera los frames en la misma carpeta
    donde están los videos.
    Args:
      n_parches:
        numero de parches por frame
    """
    fp = h5py.File(OUT_PATH, 'w')
    fp.create_dataset('X', (1500000, 3, 64, 64), dtype=np.uint8)

    ix_p = []
    jp = 0

    # Se filtran los videos
    videos_f, _ = list_video_fotografos_files_filtrado(FILTRO_FOTOGRAFOS_PATH, VIDEOS_FOTOGRAFOS_PATH)

    for videoname in videos_f:
        video_number = videoname[-8:-4]

        #Se obtienen los frames del video
        print('Obteniendo frames de ' + videoname)
        subprocess.getoutput('mkdir ' + VIDEOS_FOTOGRAFOS_PATH + '/' + video_number)
        s = 'ffmpeg -i ' + videoname + ' ' + '-s 1920x1080 ' + VIDEOS_FOTOGRAFOS_PATH + video_number + \
            '/thumb%04d.png -hide_banner'
        subprocess.getoutput(s)

        imgs, _ = list_img_files(VIDEOS_FOTOGRAFOS_PATH + video_number)
        for filename in imgs:
            video, frame = get_video_y_frame_fotografo(filename)
            print('Obteniendo parches de ' + filename)
            im = load_img(filename)
            ima = np.asarray(im).transpose((2, 0, 1))  # channel first
            parches, ix_p_aux = get_entornos(ima, n_entornos, size_entorno, video, frame)
            for imi in parches:
                fp['X'][jp] = imi
                jp = jp + 1
            ix_p.extend(ix_p_aux)

        # Se borran los frames del disco
        subprocess.getoutput('rm -rf ' + VIDEOS_FOTOGRAFOS_PATH + video_number)

    np.savetxt(IXP_FILE_NAME, ix_p, fmt='%s')
    fp.close()
    return

def mk_all_frames():
    """ Obtiene todos los frames de VIDEOS_FOTOGRAFOS_PATH
    filtrados por FILTRO_FOTOGRAFO en
    FRAME_PATH
    """
    ix_p = []
    videos_f, _ = list_video_fotografos_files_filtrado(FILTRO_FOTOGRAFOS_PATH, VIDEOS_FOTOGRAFOS_PATH)

    for videoname in videos_f:
        video_number = videoname[-8:-4]

        # Se obtienen los frames del video
        print('Obteniendo frames de ' + videoname)
        subprocess.getoutput('mkdir ' + FRAME_PATH
                           + '/' + video_number)
        s = 'ffmpeg -i ' + videoname + ' ' + '-s 1920x1080 ' + FRAME_PATH + video_number + \
            '/thumb%04d.png -hide_banner'
        subprocess.getoutput(s)

    np.savetxt(IXP_FILE_NAME, ix_p, fmt='%s')
    return


def mk_parches_from_frames_h5( size_parche, n_parches):
    """ Genera en disco el dataset para parches h5py
    a partir de los frames que están en FRAME_PATH

    Args:
      frame_path:
        carpeta desde donde se sacan las imágenes para
        armar el dataset
      n_parches:
        cantidad de parches por frame
    """
    imgs = list_img_files(FRAME_PATH)

    fp = h5py.File(OUT_PATH, 'w')
    fp.create_dataset('X', (len(imgs)*n_parches, 3, size_parche, size_parche), dtype=np.uint8)

    ix_p = []
    jp = 0

    for filename in imgs:
        video, frame = get_video_y_frame_fotografo(filename)

        print('Obteniendo parches de ' + filename)
        im = load_img(filename)
        ima = np.asarray(im).transpose((2, 0, 1))  # channel first
        parches, ix_p_aux = get_parches(ima, n_parches, size_parche, video, frame)

        for imi in parches:
            fp['X'][jp] = imi
            jp = jp + 1
        ix_p.extend(ix_p_aux)

        np.savetxt(IXP_FILE_NAME, ix_p, fmt='%s')
    fp.close()

    return


mk_parches_from_video_h5(size_parche=224, n_parches=10)
#mk_entornos_h5(size_entorno=192, n_entornos=10)
