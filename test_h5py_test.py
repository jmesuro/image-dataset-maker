"""Para transformar desde h5 a un npy o para hacerlo con
la cantidad justas de filas (imagenes)
"""
import h5py
import numpy as np
import imageio

# -------------------------------------------------------
# Para transformar desde h5 a un npy o para hacerlo con
# la cantidad justas de filas (imagenes)
# -------------------------------------------------------
def color_grid_vis(X, nh, nw, save_path=None):
    h, w = X[0].shape[:2]
    img = np.zeros((h*nh, w*nw, 3))
    for n, x in enumerate(X):
        j = n/nw
        i = n%nw
        img[j*h:j*h+h, i*w:i*w+w, :] = x
    if save_path is not None:
        imageio.imwrite(save_path, img)
    return img


def make_generator(path, n_files, batch_size):
    epoch_count = [1]
    fp = h5py.File(path, 'r')
    all_unlab_data_h5 = fp["X"]
    def get_epoch():
        images = np.zeros((batch_size, 3, 96, 96), dtype='int32')
        files = range(n_files)
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1
        for n,idx in enumerate(files):
            image = all_unlab_data_h5[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                yield (images,)
    return get_epoch


file_path = '/home/jmesuro/make_soja_datasets/output_unlabeled/test.h5'
fp = h5py.File(file_path)
fp2 = h5py.File('/home/jmesuro/make_soja_datasets/output_unlabeled/test2.h5', 'w')
fp2.create_dataset('X', (1001, 3, 96, 96), dtype=np.uint8)

jp = 0
for imi in fp['X']:
    if jp <= 99:
        fp2['X'][jp] = imi
    jp = jp + 1

fp2.close()


# (train_gen,) = (make_generator(file_path,481, 64),)


# def inf_train_gen():
#     while True:
#         for (images,) in train_gen():
#             yield images
#
# images = inf_train_gen().next()
#
# color_grid_vis(images.transpose(0, 2, 3, 1), (8, 8),
#                '/home/jmesuro/make_soja_datasets/output_unlabeled/real_%s_sample.png' % ('soja2'))

# a = np.array(fp['X'])
# np.save('/home/jmesuro/datasets/soja_tesina/parches_u2.npy', a)

fp.close()

fp2.close()

