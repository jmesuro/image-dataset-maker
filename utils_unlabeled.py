import random as rnd
from PIL import Image
import os
from scipy import misc
from PIL import Image as pil_image


def img2arrayRGB(path):
    X = misc.imread(path)
    if len(X.shape) < 2:
        print ("Error al cargar la imagen RGB: " + path)
        # import os
        # os.remove(path)
    return X


# Carga las imagenes en escala de grises manteniendo la estructura pero poniendo un 1 en el parametro de canales
def img2arrayGS(path):
    X = misc.imread(path)
    if len(X.shape) < 1:
        print ("Error al cargar la imagen GS: " + path)
    nX = X.reshape((X.shape[0], X.shape[1], 1))
    return nX


# Obtiene informacion de la imagen y en base a eso ejecuta un modo u otro (RGB o Escala de Grises)
def img2array(path):
    try:
        imagen = Image.open(path)
        if imagen.mode == 'L':
            return img2arrayGS(path)
        if imagen.mode == 'RGB':
            return img2arrayRGB(path)
        pass
    except IOError:
        print ("IOError al cargar la imagen: " + path)
        pass


def load_img(path, grayscale=False, target_size=None):
    if pil_image is None:
        raise ImportError('Could not import PIL.Image. '
                          'The use of `array_to_img` requires PIL.')
    img = pil_image.open(path)
    if grayscale:
        if img.mode != 'L':
            img = img.convert('L')
    else:
        if img.mode != 'RGB':
            img = img.convert('RGB')
    if target_size:
        wh_tuple = (target_size[1], target_size[0])
        if img.size != wh_tuple:
            img = img.resize(wh_tuple)
    return img


# ------------------------------------------------------
# LISTA LOS VIDEOS DE UN DIRECTORIO
#
# directory::  carpeta con las videos
# ------------------------------------------------------
def list_video_files(directory):
    files = []
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            path = os.path.abspath(os.path.join(dirpath, f))
            if os.path.isfile(path) and ((".MP4" in f) or (".MOV" in f)):
                files.append(path)
    return files, len(files)


# ------------------------------------------------------
# LISTA LAS IMAGENES DE UN DIRECTORIO
#
# directory::  carpeta con las imagenes
# ------------------------------------------------------
def list_img_files(directory):
    files = []
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            path = os.path.abspath(os.path.join(dirpath, f))
            if os.path.isfile(path) and (".jpg" in f or ".png" in f):
                files.append(path)
    return files, len(files)


# ----------------------------------------------------------
# LISTA LAS IMAGENES DE UN DIRECTORIO
#
# filtro_videos_fotografos_path::   filtro de los videos
# path_videos_forografos::          carpeta con las imagenes
# ----------------------------------------------------------
def list_video_fotografos_files_filtrado(filtro_videos_fotografos_path, path_videos_forografos):
    f = open(filtro_videos_fotografos_path)
    videos_filtrados = []
    for line in f:
        l2 = (line.split(' ')[1])[3:-1]
        videos_filtrados.append(l2)

    videos_path, n_videos = list_video_files(path_videos_forografos)

    videos_filtrados_path = []
    n_videos = 0
    for video in videos_path:
        if video[-12:] in videos_filtrados:
            videos_filtrados_path.append(video)
            n_videos = n_videos + 1

    return videos_filtrados_path, n_videos


# ----------------------------------------------------------
# LISTA LAS IMAGENES DE UN DIRECTORIO
#
# filtro_videos_robot_path::   filtro de los videos
# path_videos_robot::          carpeta con las imagenes
# ----------------------------------------------------------
def list_video_robot_files_filtrado(filtro_videos_robot_path, path_videos_robot):
    f = open(filtro_videos_robot_path)
    videos_filtrados = []
    for line in f:
        l2 = line.split(' ')[0]
        videos_filtrados.append(l2)

    videos_path, n_videos = list_video_files(path_videos_robot)

    videos_filtrados_path = []
    n_videos = 0
    for video in videos_path:
        if video[-24:] in videos_filtrados:
            videos_filtrados_path.append(video)
            n_videos = n_videos + 1

    return videos_filtrados_path, n_videos


# ----------------------------------------------------------
# OBTIENE LOS PARCHES DE UN ARRAY-FRAME
#
# la imagen es 1920x1080 y se obtienen parches desde el ROI
# x puede valer el rango: 504 -> 1079-s-1
# y puede valer el rango: 224 -> 1919-224-s-1
#
# ima::             array que es una imagen
# n::               cantidad de parches por frame
# s                 tamanio del parche
# video::           nombre del video
# frame:            nombre del frame
# ----------------------------------------------------------
def get_parches(a_im, n, s, video, frame):
    l = []
    lis = []
    for i in range(n):
        li = []
        x = rnd.randint(504, 1079 - s - 1)
        y = rnd.randint(224, 1919 - 224 - s - 1)
        l.append(a_im[:, x:x + s, y:y + s])  # channel first
        li.append(video)
        li.append(frame)
        li.append(x)
        li.append(y)
        li.append(s)
        lis.append(li)
    return l, lis


# ----------------------------------------------------------
# OBTIENE LOS PARCHES DE UN ARRAY-FRAME
#
# la imagen es 1920x1080 y se obtienen parches desde el ROI
# nuevo la franja inferior de la imagen
# x puede valer el rango: 504 -> 1079-s-1
# y puede valer el rango: 224 -> 1919-224-s-1
# son 30 frames a lo ancho, 270 en total
# ima::             array que es una imagen
# n::               cantidad de parches por frame
# s                 tamanio del parche
# video::           nombre del video
# frame:            nombre del frame
# ----------------------------------------------------------
def get_parches_ROI_2(a_im, n, s, video, frame):
    l = []
    lis = []
    for i in range(n):
        li = []
        x = rnd.randint(504, 1079 - s - 1)
        y = rnd.randint(0, 1919 - s - 1)
        l.append(a_im[:, x:x + s, y:y + s])  # channel first
        li.append(video)
        li.append(frame)
        li.append(x)
        li.append(y)
        li.append(s)
        lis.append(li)
    return l, lis

# ----------------------------------------------------------
# OBTIENE LOS PARCHES DE UN ARRAY-FRAME
#
# la imagen es 1920x1080 y se obtienen parches desde el ROI
# x puede valer el rango: 504 -> 1079-s-1
# y puede valer el rango: 224 -> 1919-224-s-1
#
# ima::             array que es una imagen
# n::               cantidad de parches por frame
# s                 tamanio del parche
# video::           nombre del video
# frame:            nombre del frame
# ----------------------------------------------------------
def get_parches(a_im, n, s, video, frame):
    l = []
    lis = []
    for i in range(n):
        li = []
        x = rnd.randint(504, 1079 - s - 1)
        y = rnd.randint(224, 1919 - 224 - s - 1)
        l.append(a_im[:, x:x + s, y:y + s])  # channel first
        li.append(video)
        li.append(frame)
        li.append(x)
        li.append(y)
        li.append(s)
        lis.append(li)
    return l, lis

# toma el entorno lo devuelve 64 x 64
def get_entornos(a_im, n, s, video, frame):
    l = []
    lis = []
    for i in range(n):
        li = []
        x = rnd.randint(504, 1079 - s - 1)
        y = rnd.randint(224, 1919 - 224 - s - 1)
        entorno = a_im[:, x:x + s, y:y + s]
        entorno_64 = (misc.imresize(entorno, size=(64, 64))).transpose((2, 0, 1))
        l.append(entorno_64)  # channel first
        li.append(video)
        li.append(frame)
        li.append(x)
        li.append(y)
        li.append(s)
        lis.append(li)
    return l, lis

# ----------------------------------------------------------
# OBTIENE DOS NUMEROS (VIDEO,FRAME) PARA EL PARCHE
#
# filename::    nombre del archivo imagen frame
# ----------------------------------------------------------
def get_video_y_frame_fotografo(filename):
    return int(filename[-18:-14]), int(filename[-8:-4])


# ----------------------------------------------------------
# OBTIENE DOS NUMEROS (VIDEO,FRAME) PARA EL PARCHE
#
# filename::    nombre del archivo imagen frame
# ----------------------------------------------------------
def get_video_y_frame_robot(filename):
    return str(filename[-17:-14]) + str(filename[-13:-10]), int(filename[-9:-4])


# ----------------------------------------------------------
# OBTIENE DOS NUMEROS (VIDEO,FRAME) PARA EL PARCHE
#
# filename::    nombre del archivo imagen frame
# ----------------------------------------------------------
def get_video_y_frame_robot_2d0_eti(filename):
    return  str(filename[-13:-10]), int(filename[-9:-4])



# --------------------------------------------------------------------
# DECIDE SI UN PARCHE PNG PASA EL FILTRO
# el nombre del parche contiene el nombre del frame
#
# im::                  str nombre del parche png
# filtro_robot ::       lista el numero de los videos permitidos
#                       ej [115546_003]
# filtro_fotografos::   lista el numero de los videos permitidos
#                       ej [2616]
# --------------------------------------------------------------------
def filtrar_im(im, filtro_fotografos, filtro_robot):
    if im[0:4] in filtro_fotografos:
        return True
    if str(im[0:6]) + '_' + str(im[6:9]) in filtro_robot:
        return True
    return False


# ------------------------------------------------------------
# LISTA LAS IMAGENES DE UN DIRECTORIO PERO FILTRADAS
#
# directory::           carpeta con las imagenes
# filtro_robot ::       lista el numero de los videos permitidos
#                       ej [115546_003]
# filtro_fotografos::   lista el numero de los videos permitidos
#                       ej [2616]
# ------------------------------------------------------------
def list_img_files_con_filtro(directory, filtro_fotografos, filtro_robot):
    files = []
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            path = os.path.abspath(os.path.join(dirpath, f))
            if os.path.isfile(path) and (".jpg" in f or ".png" in f) and filtrar_im(f, filtro_fotografos, filtro_robot):
                files.append(path)
    return files, len(files)


# ------------------------------------------------------------------------------------------
# ARMA UNA LISTA DE IMAGENES(FRAMES) ACEPTADAS
#
# filtro_video_fotografos_path::  archivo donde estan los videos aceptados de los fotografos
# filtro_video_robot_path::       archivo donde estan los videos aceptados de los robots
# -------------------------------------------------------------------------------------------
def makes_filtros(filtro_video_fotografos_path, filtro_video_robot_path):
    f_v2f_fotografos = open(filtro_video_fotografos_path)
    filtro_fotografos = []
    for line in f_v2f_fotografos:
        video_number = (line.split(' ')[1])[-9:-5]
        filtro_fotografos.append(video_number)

    f_v2f_robot = open(filtro_video_robot_path)
    filtro_robot = []
    for line in f_v2f_robot:
        video_number = (line.split(' ')[0])[-14:-4]
        filtro_robot.append(video_number)

    f_v2f_robot.close()
    f_v2f_fotografos.close()
    return filtro_fotografos, filtro_robot

