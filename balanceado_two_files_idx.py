"""Toma dos datasets de imagenes con sus indices y etiquetas
y lo balancea en uno.
Balancea por la clase de menor cantidad.
Elige imagenes de las otras clases de manera aleatoria.

Returns:
    parches, labels, idx balanceado
"""

import numpy as np
import random

SIZE = 96

parches1 = np.load('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/fot296/parches_labeled_(4185, 96, 96, 3)_uint8.npy')
labels1 = np.load('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/fot296/labels_(4185, 3)_3_clases_float64_maleza.npy')
idx1 = (open('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/fot296/idx_parches_labeled_(4185, 96, 96, 3)_uint8.npy')).readlines()

parches2 = np.load('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/fotrob196/parches_labeled_(10475, 96, 96, 3)_uint8.npy')
labels2 = np.load('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/fotrob196/labels_(10475, 3)_3_clases_float64_maleza.npy')
idx2 = (open('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/fotrob196/idx_parches_labeled_(10475, 96, 96, 3)_uint8.npy')).readlines()

# from robot van si o si
maleza_1 = sum(labels1)[0]
soja_1 = sum(labels1)[1]
suelo_1 = sum(labels1)[2]

# from fotografo, hasta balancear
maleza_2 = sum(labels2)[0]
soja_2 = sum(labels2)[1]
suelo_2 = sum(labels2)[2]


bolsa = range(0,parches2.shape[0])
malezas = maleza_1
suelos = suelo_1
sojas = soja_1
balance = int(min([maleza_1 + maleza_2, soja_1 + soja_2, suelo_1 + suelo_2]))


parches_balanceado = np.zeros(shape=(balance*3,SIZE,SIZE,3), dtype=np.uint8)
labels_balanceado =  np.zeros(shape=(balance*3,3), dtype=np.float64 )
idx_balanceado = [None]*balance*3

parches_balanceado[:parches1.shape[0]] = parches1
labels_balanceado[:labels1.shape[0]] = labels1
idx_balanceado[:len(idx1)] = idx1[:]

# [maleza, soja, suelo]


def balanceado():
    return malezas == balance and suelos == balance and sojas == balance

i_balanceado = parches1.shape[0]
while not balanceado():
    ir = random.choice(bolsa)
    bolsa.remove(ir)

    if labels2[ir][0] == 1:  # es maleza
        if malezas == balance:
            continue
        malezas += 1
    if labels2[ir][1] == 1:  # es suelo
        if sojas == balance:
            continue
        sojas += 1
    if labels2[ir][2] == 1:  # es soja
        if suelos == balance:
            continue
        suelos += 1

    parches_balanceado[i_balanceado] = parches2[ir]
    labels_balanceado[i_balanceado] = labels2[ir]
    idx_balanceado[i_balanceado] = idx2[ir]
    #print i_balanceado

    i_balanceado += 1


np.save('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/balanceado/parches_labeled_(' + str(balance*3) +
        ',96,96,3)_uint8_BALANCEADO_TRAIN.npy', parches_balanceado)

np.save('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/balanceado/labels_(' + str(balance*3)
        + ',3)_3_clases_float64_BALANCEADO_TRAIN.npy', labels_balanceado)

np.savetxt('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/balanceado/idx.npy', idx_balanceado, fmt='%s',newline='')  # indice


