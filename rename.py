"""
normaliza los bombre de los frames de robot.
normaliza los números para el parseo. Es decir
agrega ceros a la izquierda.

"""
import os
from subprocess import call


def list_files(directory):
    files = []
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            path = os.path.abspath(os.path.join(dirpath, f))
            if os.path.isfile(path) and (".jpg" in f or ".png" in f):
                files.append(path)
    return files, len(files)


l, _ = list_files('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/all_frames_robot_cuted_to_mark/2017_1226_122539_005/')


n_video = '005'
numero_comienzo = 0


def int_to_string(i):
    if i < 10:
        return '0000' + str(i)
    if i >= 10 and i < 100:
        return '000' + str(i)
    if i >= 100 and i < 1000:
        return '00' + str(i)
    if i >= 1000 and i < 10000:
        return '0' + str(i)
    return str(i)


def is_int(c):
    return c == '0' or c == '1' or c == '2' or c == '3' or c == '4' or c == '5' or c == '6' or c == '7' or c == '8' or \
           c == '9'


for s in l:

    if is_int(s[-9]):  # 5 cifras
        number = int(s[-9:-4]) + numero_comienzo
        path = s[:-15]
    else:
        if is_int(s[-8]):  # cifras
            number = int(s[-8:-4]) + numero_comienzo
            path = s[:-14]
        else: # 3 cifras
            number = int(s[-7:-4]) + numero_comienzo
            path = s[:-13]
    number_str = int_to_string(number)
    name = 'image-' + n_video + '-' + number_str + '.png'
    os.system('mv ' + s + ' ' + path + name)

x = 3


# outpath = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/all_frames_robot_cuted_to_mark/2017_1226_122319_006/out/'
# for s in l:
#
#     number = int(s[-7:-4])  # OJO CAMBIAR ESTO SI ES TRES O CUATRO CIFRAS
#
#     number_str = int_to_string(number)
#     name = 'image-' + number_str + '.png'
#     os.system('mv ' + s + ' ' + outpath + name)
#
# x = 3