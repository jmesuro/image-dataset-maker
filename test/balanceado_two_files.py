
import numpy as np
import random



parches1 = np.load('/home/jmesuro/datasets/soja/labeled_224/parches_labeled_(1728,224,224,3)_uint8_TEST.npy')
labels1 = np.load('/home/jmesuro/datasets/soja/labeled_224/labels_(1728,3)_3_clases_float64_TEST.npy')

parches2 = np.load('/home/jmesuro/datasets/soja/labeled_224/parches_labeled_(10711,224,224,3)_uint8_TRAIN.npy')
labels2 = np.load('/home/jmesuro/datasets/soja/labeled_224/labels_(10711,3)_3_clases_float64_TRAIN.npy')

# from robot van si o si
maleza_1 = sum(labels1)[0]
soja_1 = sum(labels1)[1]
suelo_1 = sum(labels1)[2]

# from fotografo, hasta balancear
maleza_2 = sum(labels2)[0]
soja_2 = sum(labels2)[1]
suelo_2 = sum(labels2)[2]


bolsa = range(0,parches2.shape[0])
malezas = maleza_1
suelos = suelo_1
sojas = soja_1
balance = int(min([maleza_1 + maleza_2, soja_1 + soja_2, suelo_1 + suelo_2]))


parches_balanceado = np.zeros(shape=(balance*3,224,224,3), dtype=np.uint8)
labels_balanceado =  np.zeros(shape=(balance*3,3), dtype=np.float64 )

parches_balanceado[:parches1.shape[0]] = parches1
labels_balanceado[:labels1.shape[0]] = labels1

# [maleza, soja, suelo]


def balanceado():
    return malezas == balance and suelos == balance and sojas == balance

i_balanceado = parches1.shape[0]
while not balanceado():
    ir = random.choice(bolsa)
    bolsa.remove(ir)

    if labels2[ir][0] == 1:  # es maleza
        if malezas == balance:
            continue
        malezas += 1
    if labels2[ir][1] == 1:  # es suelo
        if sojas == balance:
            continue
        sojas += 1
    if labels2[ir][2] == 1:  # es soja
        if suelos == balance:
            continue
        suelos += 1

    parches_balanceado[i_balanceado] = parches2[ir]
    labels_balanceado[i_balanceado] = labels2[ir]

    print i_balanceado

    i_balanceado += 1


np.save('/home/jmesuro/datasets/soja/labeled_224/parches_labeled_(' + str(balance*3) +
        ',224,224,3)_uint8_BALANCEADO_TRAIN.npy', parches_balanceado)

np.save('/home/jmesuro/datasets/soja/labeled_224/labels_(' + str(balance*3)
        + ',3)_3_clases_float64_BALANCEADO_TRAIN.npy', labels_balanceado)



