""" Permite armar los dataset de robot no etiquetados,
de parches y entornos.

Este módulo no está completo, probablemente haya que adaptar del de fotógrafos
a robot!!!
"""
import numpy as np
import os
import h5py
import subprocess

from utils_unlabeled import list_img_files, get_video_y_frame_robot, load_img, get_parches, get_video_y_frame_robot_2d0_eti


VIDEOS_ROBOT_PATH = '/home/jmesuro/videos_soja/robot_videos_MOV/frames_fullhd_robot/'
FILTRO_ROBOT_PATH = ''

OUT_PATH = '/home/jmesuro/make_soja_datasets/output_unlabeled/'
IXP_FILE_NAME = OUT_PATH + 'index_parches_robot_u.npz'
FRAME_PATH = ''


N_PARCHES = 10  # Cantidad de parches po framer
S_PARCHES = 96  # Tamanio del parche

# Se crea la carpeta output si no existe
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)


def mk_parches_h5(videos_robot_path=VIDEOS_ROBOT_PATH, size_parche=S_PARCHES, n_parches=N_PARCHES):

    fp = h5py.File(OUT_PATH + 'parches_robot_u.h5', 'w')
    fp.create_dataset('X', (1500000, 3, 96, 96), dtype=np.int8)

    ix_p = []
    jp = 0

    # Se filtran los videos
    imgs, _ = list_img_files(videos_robot_path)
    for filename in imgs:
        video, frame = get_video_y_frame_robot(filename)
        print('Obteniendo parches de ' + filename)
        im = load_img(filename)
        ima = np.asarray(im).transpose((2, 0, 1))  # channel first
        parches, ix_p_aux = get_parches(ima, n_parches, size_parche, video, frame)
        for imi in parches:
            fp['X'][jp] = imi
            jp = jp + 1
        ix_p.extend(ix_p_aux)

    np.savetxt(IXP_FILE_NAME, ix_p, fmt='%s')
    fp.close()
    return


def mk_parches_h5(size_parche, n_parches):

    imgs = list_img_files('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/all_frames_robot_cuted_to_mark/2017_1226_122319_006/')

    fp = h5py.File(OUT_PATH + 'parches_robot_u_2017_1226_122319_006.h5', 'w')
    fp.create_dataset('X', (len(imgs)*n_parches, 3, size_parche, size_parche), dtype=np.uint8)

    ix_p = []
    jp = 0

    for filename in imgs:
        video, frame = get_video_y_frame_robot_2d0_eti(filename)

        print('Obteniendo parches de ' + filename)
        im = load_img(filename)
        ima = np.asarray(im).transpose((2, 0, 1))  # channel first
        parches, ix_p_aux = get_parches(ima, n_parches, size_parche, video, frame)

        for imi in parches:
            fp['X'][jp] = imi
            jp = jp + 1
        ix_p.extend(ix_p_aux)

        np.savetxt(IXP_FILE_NAME, ix_p, fmt='%s')
    fp.close()

    return

mk_parches_h5()
