import numpy as np
from PIL import Image as pil_image
import commands
import os

from utils_unlabeled import list_video_files, list_img_files, get_video_y_frame_fotografo, get_video_y_frame_robot, \
    load_img, get_parches, makes_filtros, list_img_files_con_filtro, img2array, list_video_fotografos_files_filtrado,\
    list_video_robot_files_filtrado


# ---------------------------------------------------------------
# DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED
# DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED
# DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED
# DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED
# ---------------------------------------------------------------


VIDEOS_FOTOGRAFOS_PATH = '/home/jmesuro/videos_soja/DJI_videos_MOV/'
VIDEOS_ROBOT_PATH = '/home/jmesuro/videos_soja/robot_videos_MOV/frames_fullhd_robot/'

PATH = '/home/jmesuro/make_soja_datasets/'  # folder donde esta este proyecto

FILTRO_FOTOGRAFOS_PATH = PATH + 'input_unlabeled/filtro_videos_fotografos.txt'
FILTRO_ROBOT_PATH = PATH + 'input_unlabeled/filtro_videos_robot.txt'

OUT_PATH = '/home/jmesuro/make_soja_datasets/output_unlabeled/'
PARCHES_NPY_FILE_NAME = OUT_PATH + 'parches_u.npy'
IXP_FILE_NAME = OUT_PATH + 'index_parches_u.npz'
PARCHES_PNG_PATH = OUT_PATH + 'parches_png/'

N_PARCHES = 10  # Cantidad de parches po framer
S_PARCHES = 64  # Tamanio del parche

# Se crea la carpeta output si no existe
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)


# -----------------------------------------------------------------------------
# HACE LOS PARCHES PNG
#
# Utiliza los videos robot y fotografos para armar los parches
# VIDEOS_FOTOGRAFOS_PATH, VIDEOS_ROBOT_PATH -> PARCHES_PNG_PATH, IXP_FILE_NAME
# nombre de los parches VIDEO_FRAME_X_Y.png
#
# size_parche:: tamanio del parche
# n_parches::   cantidad de parches por frame
# -----------------------------------------------------------------------------
def mk_parches_png(videos_fotografos_path=VIDEOS_FOTOGRAFOS_PATH, videos_robot_path=VIDEOS_ROBOT_PATH,
                   size_parche=S_PARCHES, n_parches=N_PARCHES):

    ix_p = []
    parches = []

    videos_f, n_videos_f = list_video_fotografos_files_filtrado(FILTRO_FOTOGRAFOS_PATH, videos_fotografos_path)
    # videos_r, n_videos_r = list_video_robot_files_filtrado(FILTRO_ROBOT_PATH, videos_robot_path)

    # videos_f, n_videos_f = list_video_files(videos_fotografos_path)
    # videos_r, n_videos_r = list_video_files(videos_robot_path)
    videos_f = ['/home/jmesuro/videos_soja/DJI_videos_MOV/DJI_2616.MOV']
    for videoname in videos_f:
        video_number = videoname[-8:-4]

        # Se obtienen los frames del video
        print('Obteniendo frames de ' + videoname)
        commands.getoutput('mkdir ' + VIDEOS_FOTOGRAFOS_PATH + '/' + video_number)
        s = 'ffmpeg -i ' + videoname + ' ' + '-s 1920x1080 ' + videos_fotografos_path + video_number + \
            '/thumb%04d.png -hide_banner'
        commands.getoutput(s)

        imgs, _ = list_img_files(videos_fotografos_path + video_number)
        for filename in imgs:
            video, frame = get_video_y_frame_fotografo(filename)
            print('Obteniendo parches de ' + filename)
            im = load_img(filename)
            ima = np.asarray(im).transpose((2, 0, 1))  # channel first
            p_aux, ix_p_aux = get_parches(ima, n_parches, size_parche, video, frame)
            parches.extend(p_aux)
            ix_p.extend(ix_p_aux)

        # Se borran los frames del disco
        commands.getoutput('rm -rf ' + videos_fotografos_path + video_number)
    #
    # for videoname in videos_r:
    #     video_number = videoname[-14:-4]

        # Se obtienen los frames del video
        # print('Obteniendo frames de ' + videoname)
        # commands.getoutput('mkdir ' + VIDEOS_ROBOT_PATH + '/' + video_number)
        # s = 'ffmpeg -i ' + videoname + ' ' + '-s 1920x1080 ' + VIDEOS_ROBOT_PATH + video_number + \
        #     '/thumb%04d.png -hide_banner'
        # commands.getoutput(s)

    # imgs, _ = list_img_files(VIDEOS_ROBOT_PATH)
    # for filename in imgs:
    #     video, frame = get_video_y_frame_robot(filename)
    #     print('Obteniendo parches de ' + filename)
    #     im = load_img(filename)
    #     ima = np.asarray(im).transpose((2, 0, 1))  # channel first
    #     p_aux, ix_p_aux = get_parches(ima, n_parches, size_parche, video, frame)
    #     parches.extend(p_aux)
    #     ix_p.extend(ix_p_aux)

        # Se borran los frames del disco
    # commands.getoutput('rm -rf ' + VIDEOS_ROBOT_PATH)

    if not os.path.exists(PARCHES_PNG_PATH):
        os.mkdir(PARCHES_PNG_PATH)

    # Se guardan en disco los parches y los indices
    i = 0
    for parche in parches:
        im = pil_image.fromarray(parche.transpose(1, 2, 0))
        # Nombre del parche VIDEO_FRAME_X_Y.png
        im.save(PARCHES_PNG_PATH + str((ix_p[i])[0]) + '_' + str((ix_p[i])[1]) + '_' + str((ix_p[i])[2]) +
                '_' + str((ix_p[i])[3]) + '.png')
        i = i + 1

    np.savetxt(IXP_FILE_NAME, ix_p, fmt='%s')
    return


# -----------------------------------------------------------------------------
# HACE EL DATASET NPY
#
# Utiliza los parches de PARCHES_PNG_PATH para armar el dataset
#
# filtro_fotografos_path::  filtro de los videos de los fotografos
# filtro_robot_path::       filtro de los videos de los robot
# parches_npy_file_name::   donde se guarda el dataset
# -----------------------------------------------------------------------------
def mk_npy(filtro_fotografos_path=FILTRO_FOTOGRAFOS_PATH, filtro_robot_path=FILTRO_ROBOT_PATH,
           parches_npy_file_name=PARCHES_NPY_FILE_NAME):

    filtro_fotografos, filtro_robot = makes_filtros(filtro_fotografos_path, filtro_robot_path)
    paths, _ = list_img_files_con_filtro(PARCHES_PNG_PATH, filtro_fotografos, filtro_robot)
    X = np.array([img2array(path) for path in paths])
    np.save(parches_npy_file_name, X)


mk_parches_png()
#mk_npy()
