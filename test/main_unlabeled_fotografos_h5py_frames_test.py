import numpy as np
import commands
import h5py
import os

from utils_unlabeled import list_img_files, get_video_y_frame_fotografo, \
    load_img, get_parches_ROI_2, list_video_fotografos_files_filtrado


VIDEOS_FOTOGRAFOS_PATH = '/home/jmesuro/videos_soja/DJI_videos_MOV/'
FILTRO_FOTOGRAFOS_PATH = '/home/jmesuro/make_soja_datasets/input_unlabeled/filtro_videos_fotografos.txt'
IXP_FILE_NAME = '/home/joaquin/index_entornos_fotografos_u_roi2.npz'
OUT_PATH = '/home/joaquin/parches_fotografos_u2_roi2.h5'


def mk_all_frames():

    ix_p = []
    videos_f, _ = list_video_fotografos_files_filtrado(FILTRO_FOTOGRAFOS_PATH, VIDEOS_FOTOGRAFOS_PATH)

    for videoname in videos_f:
        video_number = videoname[-8:-4]

        # Se obtienen los frames del video
        print('Obteniendo frames de ' + videoname)
        commands.getoutput('mkdir ' + '/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/test2'
                           + '/' + video_number)
        s = 'ffmpeg -i ' + videoname + ' ' + '-s 1920x1080 ' + '/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/test2/' + video_number + \
            '/thumb%04d.png -hide_banner'
        commands.getoutput(s)

    np.savetxt(IXP_FILE_NAME, ix_p, fmt='%s')
    return


# ------------------------------------------------------
# LISTA LAS IMAGENES DE UN DIRECTORIO
#
# directory::  carpeta con las imagenes
# ------------------------------------------------------
def list_img_files(directory):
    files = []
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            path = os.path.abspath(os.path.join(dirpath, f))
            if os.path.isfile(path) and (".jpg" in f or ".png" in f):
                files.append(path)
    return files


def mk_parches_h5(size_parche, n_parches):

    imgs = list_img_files('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/all_frames_fotografos/')

    fp = h5py.File(OUT_PATH, 'w')
    fp.create_dataset('X', (len(imgs)*n_parches, 3, size_parche, size_parche), dtype=np.uint8)

    ix_p = []
    jp = 0

    for filename in imgs:
        video, frame = get_video_y_frame_fotografo(filename)

        print('Obteniendo parches de ' + filename)
        im = load_img(filename)
        ima = np.asarray(im).transpose((2, 0, 1))  # channel first
        parches, ix_p_aux = get_parches_ROI_2(ima, n_parches, size_parche, video, frame)

        for imi in parches:
            fp['X'][jp] = imi
            jp = jp + 1
        ix_p.extend(ix_p_aux)

        np.savetxt(IXP_FILE_NAME, ix_p, fmt='%s')
    fp.close()

    return


def mk_parches_h5_by_folder(size_parche, n_parches, folder, vid):

    imgs = list_img_files(folder)

    outpath = '/home/joaquin/' + vid + '.h5'
    fp = h5py.File(outpath, 'w')
    fp.create_dataset('X', (len(imgs)*n_parches, 3, size_parche, size_parche), dtype=np.uint8)

    ix_p = []
    jp = 0

    for filename in imgs:
        video, frame = get_video_y_frame_fotografo(filename)

        print('Obteniendo parches de ' + filename)
        im = load_img(filename)
        ima = np.asarray(im).transpose((2, 0, 1))  # channel first
        parches, ix_p_aux = get_parches_ROI_2(ima, n_parches, size_parche, video, frame)

        for imi in parches:
            fp['X'][jp] = imi
            jp = jp + 1
        ix_p.extend(ix_p_aux)

        np.savetxt(IXP_FILE_NAME, ix_p, fmt='%s')
    fp.close()

    return


all_frams_dir = '/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/all_frames_fotografos'


def mk_all_vids(dir):
    l_dir = os.listdir(all_frams_dir)
    for vid in l_dir:
        path = dir + '/' + vid
        mk_parches_h5_by_folder(224, 13, path, vid)

#mk_all_vids(all_frams_dir)


dir_d = '/home/joaquin/datasets/'
outpath_d = '/home/joaquin/fot_roi2.h5'
folder_im = '/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/all_frames_fotografos/'


def concatenate(dir_d,outpath_d, long, n_parches=13,size_parche=224):



    l_dir = os.listdir(dir_d)
    fd = h5py.File(outpath_d,'w')

    fd.create_dataset('X', (long * n_parches, 3, size_parche, size_parche), dtype=np.uint8)

    l = 0
    for d in l_dir:
        di = h5py.File(dir_d + d)
        len = di['X'].shape[0]
        fd['X'][l:l+len] = di['X'][:]

    fd.close()

long = len(list_img_files(folder_im))
concatenate(dir_d=dir_d,outpath_d=outpath_d,long=long)
#mk_parches_h5(size_parche=224, n_parches=13)
#mk_entornos_h5(size_entorno=192, n_entornos=10)

#
# h1 = h5py.File('uno.h5')
# h2 = h5py.File('dos.h5')
# h1['X'][:] = np.load('uno.npy')
# h2['X'][:] = np.load('dos.npy')
# h3 = h5py.File('tres.h5')
#
# shape = #armar el shape como suma de todods los datasets
#
# d3 = h3.create_dataset('X', SHAPE=SHAPE)
#
