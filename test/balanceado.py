
import numpy as np
import random

parches = np.load('/home/jmesuro/datasets/soja/labeled_224/parches_labeled_(1728,224,224,3)_uint8_TEST.npy')
labels = np.load('/home/jmesuro/datasets/soja/labeled_224/labels_(1728,3)_3_clases_float64_TEST.npy')

bolsa = range(0,parches.shape[0])
malezas = 0
suelos = 0
sojas = 0
balance = int(min(sum(labels)))

parches_balanceado = np.zeros(shape=(balance*3,224,224,3), dtype=np.uint8)
labels_balanceado =  np.zeros(shape=(balance*3,3), dtype=np.float64 )

# [maleza, soja, suelo]


def balanceado():
    return malezas == balance and suelos == balance and sojas == balance

i_balanceado = 0
while not balanceado():
    ir = random.choice(bolsa)

    if labels[ir][0] == 1:  # es maleza
        if malezas == balance:
            continue
        malezas += 1
    if labels[ir][1] == 1:  # es suelo
        if sojas == balance:
            continue
        sojas += 1
    if labels[ir][2] == 1:  # es soja
        if suelos == balance:
            continue
        suelos += 1

    parches_balanceado[i_balanceado] = parches[ir]
    labels_balanceado[i_balanceado] = labels[ir]

    bolsa.remove(ir)
    i_balanceado += 1


np.save('/home/jmesuro/datasets/soja/labeled_224/parches_labeled_(' + str(balance*3) +
        ',224,224,3)_uint8_BALANCEADO_TEST.npy', parches_balanceado)

np.save('/home/jmesuro/datasets/soja/labeled_224/labels_(' + str(balance*3)
        + ',3)_3_clases_float64_BALANCEADO_TEST.npy', labels_balanceado)



