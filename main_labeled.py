""" Permite armar los dataset de fotografos y robot etiquetados.

Se necesitan los frames y el archivo grid-export.txt de Erica con las etiquetas

Tiene tres funciones principales:
       marks_img       marca en los frames los parches
       mk_parches_png  obtiene los parches png (pueden ser entornos tambien)
       mk_npy          arma los archivos numpy de labels, parches e indice

inputs
    FRAMES_FULLHD_PATH:
        No son frames consecutivos, son frames en una carpeta, los que se
        mostraron en el programa de etiquetado
    FILTRO_VIDEOS_FOTOGRAFOS_PATH:
        Indica los frames de que videos de fotografos deben usarse
    FILTRO_VIDEOS_ROBOT_PATH:
        Idem
    GRID_PATH:
        Indices de imagenes con las coordenadas de las etiquetas

outputs
    PARCHES_PNG_PATH:
        carpeta con los parches, el nombre de cada una mantiene
        la infotmacion de donde viene.
    FRAMES_FULLHD_PATH_M:
        carpeta con las imagenes marcadas
    PARCHES_NPY_PATH:
        carpeta donde se guardan los 3 datasets npy,
        parches, etiquetas, indice.

"""

import numpy as np
from PIL import Image
import os
from utils_labeled import mark_parche, mk_arrays_from_imgs_filtradas, get_parche, \
    parche_name, get_parche_no_bordes, mk_arrays_from_imgs

# INPUT PATHS
FRAMES_FULLHD_PATH = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/frames_1er_etiquetado/'  # imagenes 1920 1080
FILTRO_VIDEOS_FOTOGRAFOS_PATH = '/home/jmesuro/image-dataset-maker/input_labeled/filtro_videos_fotografos.txt'
FILTRO_VIDEOS_ROBOT_PATH = '/home/jmesuro/image-dataset-maker/input_labeled/filtro_videos_robot.txt'
GRID_PATH = '/home/jmesuro/image-dataset-maker/input_labeled/grid-export_1er_joa.txt'  # archivos de etiquetas

# OUTPUT PATHS
PARCHES_PNG_PATH = '/home/jmesuro/image-dataset-maker/output_labeled/png/'
FRAMES_FULLHD_PATH_M = '//home/jmesuro/image-dataset-maker/output_labeled/frames/'  # imags 1920 1080 marcadas
PARCHES_NPY_PATH = '/home/jmesuro/image-dataset-maker/output_labeled/npy/'




def marks_img(size_parche=96,format='jpg'):
    """ Marca los parche en las imagenes
    y las guarda en FRAMES_FULLHD_PATH_M

    Args
      size_parche:
        tamanio del parche
      format:
        formato de la imagen de salida jpg o png
    """
    f = open(GRID_PATH, 'r')
    f.readline()  # descarto la primera linea

    if not os.path.exists(FRAMES_FULLHD_PATH_M):
        os.mkdir(FRAMES_FULLHD_PATH_M)

    for linea in f:
        llinea = linea.split('\t')
        img_name = (llinea[1][1:-1])[0:-5] + '.png'  # le saco las comillas dobles y lo cambio a png
        if img_name != ".png":
            print(img_name)
            x = int(llinea[3][1:-1])
            y = int(llinea[4][1:-1])
            label = int(llinea[5][1:-1])
            if os.path.isfile(
                    FRAMES_FULLHD_PATH_M + img_name[:-4] + '.jpg'):  # si ya esta marcada la saco de parches marcados
                img = Image.open(FRAMES_FULLHD_PATH_M + img_name[:-4] + '.jpg')
            else:  # si no la saco de las originales
                img = Image.open(FRAMES_FULLHD_PATH + img_name)
            im_m = mark_parche(img, x, y, size_parche=size_parche, label=label)
            im_m.save(FRAMES_FULLHD_PATH_M + img_name[:-4] + '.' + format)

    f.close()


def mk_parches_png(size, bordes=False):
    """ Hace los parches png y los guarda en disco
    No usa filtros. Los hace todos.

    Args
      size:
        tamanio del parche
      bordes:
        si es True y si el parche sale de los bordes de la imagen
        se descarta,
        si es FALSE, en los bordes hay tratamiento especial
        la etiqueta no esta mas en el centro del parche.
    """
    f = open(GRID_PATH, 'r')
    f.readline()  # descarto la primera linea

    if not os.path.exists(PARCHES_PNG_PATH):
        os.mkdir(PARCHES_PNG_PATH)

    for linea in f:
        llinea = linea.split('\t')
        img_name = (llinea[1][1:-1])[0:-5] + '.png'  # le saco las comillas dobles y lo cambio a png
        if img_name != '.png':
            print(img_name)
            x = int(llinea[3][1:-1])
            y = int(llinea[4][1:-1])
            label = int(llinea[5][1:-1])
            img = Image.open(FRAMES_FULLHD_PATH + img_name)
            if bordes:
                parche, new_x, new_y = get_parche(img, x, y, size_parche=size)
            else:
                hay_parche, parche, new_x, new_y = get_parche_no_bordes(img, x, y, size_parche=size)

            if hay_parche:
                (Image.fromarray(parche)).save(PARCHES_PNG_PATH + parche_name(img_name[:-4] + '.png', new_x, new_y, label))

    f.close()


def mk_npy(size, modo, filtro=True):
    """ Hace los 3 dataset: dataset, labels, idx
    y los guarda en disco.

    Args:
      size:
        tamanio del parche
      modo:
        string 'parches', o 'entornos'
      filtro:
        True con filtro, False sin filtro
    """
    if filtro:
        x, y, idx = mk_arrays_from_imgs_filtradas(PARCHES_PNG_PATH, FILTRO_VIDEOS_FOTOGRAFOS_PATH, FILTRO_VIDEOS_ROBOT_PATH,
                                              size)
    else:
        x, y, idx = mk_arrays_from_imgs(PARCHES_PNG_PATH)

    if not os.path.exists(PARCHES_NPY_PATH):
        os.mkdir(PARCHES_NPY_PATH)

    parches_name = modo + '_labeled_' + str(x.shape) + '_' + str(x.dtype) + '.npy'
    labels_name = 'labels_' + str(y.shape) + '_3_clases_' + str(y.dtype) + '.npy'

    np.savetxt(PARCHES_NPY_PATH + 'idx_' + parches_name, idx, fmt='%s')  # indice
    np.save(PARCHES_NPY_PATH + parches_name, x)  # parches
    np.save(PARCHES_NPY_PATH + labels_name, y)  # labels

    n1 = np.zeros(shape=(y.size, 1), dtype=np.float64)  # dim1: suelo y soja son 1, maleza es 0
    for i in range(y.size):
        if y[i] == 0:
            n1[i] = 0
        if y[i] == 1:
            n1[i] = 1
        if y[i] == 2:
            n1[i] = 1

    n2 = np.zeros(shape=(y.size, 2), dtype=np.float64)  # dim2: soja y suelo es [0,1], maleza [1,0]
    for i in range(y.size):
        if y[i] == 0:
            n2[i, 0] = 1
        if y[i] == 1 or y[i] == 2:
            n2[i, 1] = 1

    n3 = np.zeros(shape=(y.size, 3), dtype=np.float64)  # dim3: suelo es [0,0,1]; soja es [0,1,0]; maleza es [1,0,0]
    for i in range(y.size):
        if y[i] == 0:
            n3[i, 0] = 1
        if y[i] == 1:
            n3[i, 1] = 1
        if y[i] == 2:
            n3[i, 2] = 1

    labels_name1 = 'labels_' + str(n1.shape) + '_2_clases_' + str(n1.dtype) + '_maleza.npy'
    labels_name2 = 'labels_' + str(n2.shape) + '_2_clases_' + str(n2.dtype) + '_maleza.npy'
    labels_name3 = 'labels_' + str(n3.shape) + '_3_clases_' + str(n3.dtype) + '_maleza.npy'

    np.save(PARCHES_NPY_PATH + labels_name1, n1)
    np.save(PARCHES_NPY_PATH + labels_name2, n2)
    np.save(PARCHES_NPY_PATH + labels_name3, n3)

marks_img(size_parche=64)
#mk_parches_png(size=96, bordes=False)
#mk_npy(size=96, modo='parches')
#mk_npy(size=224, modo='parches')
#marks_img_erica2(size_img=[1920, 1080], size_parche=224)
