import numpy as np
from PIL import Image
import os
from scipy import misc


def img2arrayRGB(path):
    """ Carga las imagenes en RGB

    Argas:
      path:
        path de la imagen
    Returns:
      numpy array de la imagen
    """
    X = misc.imread(path)
    if len(X.shape) < 2:
        print ("Error al cargar la imagen RGB: " + path)
        # import os
        # os.remove(path)
    return X


#
def img2arrayGS(path):
    """ Carga las imagenes en escala de grises manteniendo
    la estructura pero poniendo un 1 en el parametro de canales

    Argas:
      path:
        path de la imagen
    Returns:
      numpy array de la imagen
    """
    X = misc.imread(path)
    if len(X.shape) < 1:
        print ("Error al cargar la imagen GS: " + path)
    nX = X.reshape((X.shape[0], X.shape[1], 1))
    return nX

def img2array(path):
    """ Obtiene informacion de la imagen y en base a eso ejecuta
    un modo u otro (RGB o Escala de Grises)

    Argas:
      path:
        path de la imagen
    Returns:
      numpy array de la imagen
    """
    try:
        imagen = Image.open(path)
        if imagen.mode == 'L':
            return img2arrayGS(path)
        if imagen.mode == 'RGB':
            return img2arrayRGB(path)
        pass
    except IOError:
        print ("IOError al cargar la imagen: " + path)
        pass


def get_parche(im, x, y,  size_parche):
    """ Obtiene un parche en la imagen im

    Las coordenadas son del pixel central del parche referido a una imagen de 766 430,
    se transforman esas coordenadas a las correspondiente para im
    se extrae ese parche de (size x size) desde la imagen im

    En los bordes hay tratamiento especial para que el parche siempre sea de size x size

    Args:
      im:
        imagen donde se saca el parche
      x,y:
        int coordenadas centro del parche correspondiente a la im pero  de tamanio 766 430
      size_parche:
        tamanio del parche (es cuadrado size_parche x size_parche)

    Returns:
      si el parche se descarto o no,
      el parche,
      las nuevas coordenadas
    """
    parche_half_size = size_parche / 2

    nx = int(y * im.size[0] / 766)  # los ejes x e y son distintos en "im" que en "a"
    ny = int(x * im.size[1] / 430)
    a = np.asarray(im)

    up = nx - parche_half_size
    bt = nx + parche_half_size
    iz = ny - parche_half_size
    dr = ny + parche_half_size

    if nx - parche_half_size < 0:
        up = 0
        bt = size_parche
    if nx + parche_half_size > im.size[1]:
        up = im.size[1] - size_parche
        bt = im.size[1]

    if ny - parche_half_size < 0:
        iz = 0
        dr = size_parche
    if ny + parche_half_size > im.size[0]:
        iz = im.size[0] - size_parche
        dr = im.size[0]

    new_x = ny
    new_y = nx
    return a[up: bt, iz:dr, :], new_x, new_y


def get_parche_no_bordes(im, x, y,  size_parche):
    """ Obtiene un parche en la imagen im

    Las coordenadas son del pixel central del parche referido a una imagen de 766 430,
    se transforman esas coordenadas a las correspondiente para im
    se extrae ese parche de (size x size) desde la imagen im

    En los bordes, si el parche no se puede armar por estar en el borde se descarta.
    si im.size = 1920,1080 entonces a.shape = 1080 1920 3
    x e y se refieren a "im" no a "a"

    Args:
      im:
        imagen donde se saca el parche
      x,y:
        int coordenadas centro del parche correspondiente a la im pero  de tamanio 766 430
      size_parche:
        tamanio del parche (es cuadrado size_parche x size_parche)

    Returns:
      si el parche se descarto o no,
      el parche,
      las nuevas coordenadas
    """
    parche_half_size = size_parche / 2

    nx = int(y * im.size[0] / 766)  # los ejes x e y son distintos en "im" que en "a"
    ny = int(x * im.size[1] / 430)
    a = np.asarray(im)

    up = nx - parche_half_size
    bt = nx + parche_half_size
    iz = ny - parche_half_size
    dr = ny + parche_half_size

    hay_parche = True

    if (nx - parche_half_size < 0) or (nx + parche_half_size > im.size[1]) or (ny - parche_half_size < 0) or (ny + parche_half_size > im.size[0]):
        hay_parche = False

    new_x = ny
    new_y = nx

    up = int(up)
    bt = int(bt)
    iz = int(iz)
    dr = int(dr)
    return hay_parche, a[up: bt, iz:dr, :], new_x, new_y


def mark_parche(im, x, y, size_parche, label):
    """ Marca un parche en la imagen

    Las coordenadas son del pixel central del parche referido a una imagen de 766 430,
    se transforman esas coordenadas a las correspondiente para im
    se marca ese parche de (size x size) en la imagen im

    En los bordes hay tratamiento especial para que el parche siempre sea de size x size

    si im.size = 1920,1080 entonces a.shape = 1080 1920 3
    x e y se refieren a "im" no a "a"

    Args:
      im:
        imagen a marcar
      x,y:
        int coordenadas centro del parche correspondiente a 766 430
      size_parche:
        tamanio del parche (cuadrado de size_parche x size_parche)
      label:
        int del label, para el color del cuadro
            rojo    maleza
            azul    soja
            negro   suelo

    Returns:
      imagen marcada.
    """
    parche_half_size = size_parche / 2

    nx = int(y * im.size[0] / 766)  # es asi poruqe los ejes x e y son distintos en "im" que en "a"
    ny = int(x * im.size[1] / 430)
    a = np.asarray(im)

    up = nx - parche_half_size
    bt = nx + parche_half_size
    iz = ny - parche_half_size
    dr = ny + parche_half_size

    if nx - parche_half_size < 0:
        up = 0
        bt = size_parche
    if nx + parche_half_size > im.size[1]:
        up = im.size[1] - size_parche
        bt = im.size[1]

    if ny - parche_half_size < 0:
        iz = 0
        dr = size_parche
    if ny + parche_half_size > im.size[0]:
        iz = im.size[0] - size_parche
        dr = im.size[0]

    r = np.array(a, dtype=np.uint8)

    # Arma recuadro en la imagen original, rojo:maleza, azul:soja, negro:suelo
    # Linea de arriba
    up = int(up)
    iz = int(iz)
    dr = int(dr)
    bt = int(bt)

    r[up: up + 4, iz:dr, 0] = 255 if label == 0 else 0
    r[up: up + 4, iz:dr, 1] = 0
    r[up: up + 4, iz:dr, 2] = 255 if label == 1 else 0

    # Linea de abajo
    r[bt - 4:bt, iz:dr, 0] = 255 if label == 0 else 0
    r[bt - 4:bt, iz:dr, 1] = 0
    r[bt - 4:bt, iz:dr, 2] = 255 if label == 1 else 0

    # Linea de izquierda
    r[up: bt, iz:iz + 4, 0] = 255 if label == 0 else 0
    r[up: bt, iz:iz + 4, 1] = 0
    r[up: bt, iz:iz + 4, 2] = 255 if label == 1 else 0

    # Linea de derecha
    r[up: bt, dr - 4:dr, 0] = 255 if label == 0 else 0
    r[up: bt, dr - 4:dr, 1] = 0
    r[up: bt, dr - 4:dr, 2] = 255 if label == 1 else 0

    # r[up: up + 3, iz:dr, 1] = 0
    # r[up: bt, iz:dr, 2] = 0
    return Image.fromarray(r)


def parche_name(img_name, x, y, label):
    """ Arma el nombre de los parches png
    Agrega el nombre de la imagen, las coordenadas y la etiqueta

    Args:
      img_name:
        string con el nombre original de la imagen
      x,y:
        ints con las coordenadas
      label:
        int con el label
    Returns:
      string con el nombre de la imagen
    """
    slabel = ''
    if label == 0:
        slabel = '|0|maleza'
    if label == 1:
        slabel = '|1|soja'
    if label == 2:
        slabel = '|2|suelo'
    return img_name[0:-4] + '_label=' + slabel + '_coords=' + str(x) + '_' + str(y) + '.png'


def int_to_string(i):
    """ Transforma un int con formato a un string
    ej:   1 ->  0001
         11 ->  0011
        111 ->  0111
       1111 ->  1111
   """
    if i < 10:
        return '000' + str(i)
    if i >= 10 and i < 100:
        return '00' + str(i)
    if i >= 100 and i < 1000:
        return '0' + str(i)
    if i >= 1000:
        return str(i)

def makes_filtros(filtro_video_fotografos_path, filtro_video_robot_path):
    """ Arma una lista de imagenes (frames) aceptadas
    La lista tiene los nombres de la simagenes ej: image-0011.jpg

    Args:
      filtro_video_fotografos_path:
        archivo donde estan los videos aceptados de los fotografos
      filtro_video_robot_path:
        archivo donde estan los videos aceptados de los robots

    Returns:
      dos listas que corresponden a los filtros
    """
    f_v2f_fotografos = open(filtro_video_fotografos_path)
    filtro_fotografos = []
    for line in f_v2f_fotografos:
        i1 = int((line.split(' ')[2]).split('-')[0])
        i2 = int((line.split(' ')[2]).split('-')[1])
        for i in range(i1, i2+1):
            s = 'image-' + int_to_string(i)
            filtro_fotografos.append(s)

    f_v2f_robot = open(filtro_video_robot_path)
    filtro_robot = []
    for line in f_v2f_robot:
        modo = ((line.split(' '))[1])[0:-1]  # RGB o INF
        n_imagen = ((line.split(' '))[0])[-7:-4]
        filtro_robot.append(modo + '-' + n_imagen)

    f_v2f_robot.close()
    f_v2f_fotografos.close()
    return filtro_fotografos, filtro_robot


def filtrar_im(im, filtro_fotografos, filtro_robot):
    """ Devuelve si un parche png pasa o no el filtro

    Args:
      im:
        str nombre del parche png
      filtro_robot:
        lista con la pinta de las imagenes aceptadas
        ej: [RGB-001,INF-002]
      filtro_fotografos:
        lista con los nombres de los frames aceptados
    """
    if im[0:10] in filtro_fotografos:
        return True
    if im[5:12] in filtro_robot:
        return True
    return False


def list_img_files_con_filtro(directory, filtro_fotografos, filtro_robot):
    """ Lista las imagenes de un directorio pero filtradas

    Args:
      directory:
        carpeta con las imagenes
      imagenes_filtradas:
        imagenes que si se van a listar
      filtro_robot:
        lista con la pinta de las imagenes aceptadas
        ej: [RGB-001,INF-002]
      filtro_fotografos:
        lista con los nombres de los frames aceptados

    Returns:
      lista con los path de los archivos
    """
    files = []
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            path = os.path.abspath(os.path.join(dirpath, f))
            if os.path.isfile(path) and (".jpg" in f or ".png" in f) and filtrar_im(f, filtro_fotografos, filtro_robot):
                files.append(path)
    return files


def list_img_files(directory):
    """ Lista las imagenes de un directorio

    Args
      directory:
        carpeta con las imagenes

    Returns:
      lista con los path de los archivos
    """
    files = []
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            path = os.path.abspath(os.path.join(dirpath, f))
            if os.path.isfile(path) and (".jpg" in f or ".png" in f):
                files.append(path)
    return files


def get_label(name):
    """ Obtiene el label del nombre del parche

    Args
      name:
        es un string con el nombre del parche

    Returns:
      un int que corresponde al label
    """
    label = (name.split('|'))[1]
    return int(label)


def mk_arrays_from_imgs(parches_png_path):
    """ Hace las matrices numpy de parches y labels
    obtiene los labels del nombre de los parches png

    Args:
      parches_png_path:
        carpeta donde estan los png

    Returns:
      tres matrices numpy con los dataset: parches, label, idx
    """
    paths = list_img_files(parches_png_path)
    X = np.array([img2array(path) for path in paths])
    Y = np.array([get_label(path) for path in paths])
    idx = [line[60:] for line in paths]
    return X, Y, idx


def mk_arrays_from_imgs_filtradas(parches_png_path, filtro_fotografos_path, filtro_robot_path, size):
    """ Hace las matrices numpy de parches y labels
    obtiene los labels del nombre de los parches png
    Filtra las que imagenes que no estan en los filtros

    Args:
      parches_png_path:
        carpeta donde estan los png
      filtro_fotografos_path:
        archivo donde estan los videos aceptados de los fotografos
      filtro_robot_path:
        archivo donde estan los videos aceptados de los robots
      size:
        tamanio del parche

    Returns:
      tres matrices numpy con los dataset: parches, label, idx
    """
    filtro_fotografos, filtro_robot = makes_filtros(filtro_fotografos_path, filtro_robot_path)
    paths = list_img_files_con_filtro(parches_png_path, filtro_fotografos, filtro_robot)
    # idx indice de los parches en el npy
    idx = [line[60:] for line in paths]
    # X npy con los parches
    X = np.array([misc.imresize(img2array(path), size=(size, size)) for path in paths])
    # Y npy con los labels
    Y = np.array([get_label(path) for path in paths])
    return X, Y, idx


